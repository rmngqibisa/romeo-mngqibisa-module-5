import 'dart:html';
import 'dart:ui';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized;
  await Firebase.initializeApp();
  runApp(const MyApp());
}

const appTitle = "Readington.X";
const cPrimary = Colors.deepPurple;
const cPrimaryLight = Color(0xFFF1E6FF);

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        primarySwatch: cPrimary,
        backgroundColor: cPrimaryLight,
      ),
      home: SplashScreen(
          seconds: 8,
          navigateAfterSeconds: const WelcomeScreen(
            title: 'Welcome to Readington.X',
          ),
          title: const Text(
            'READINGTON.X',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
                color: cPrimaryLight),
          ),
          backgroundColor: cPrimary,
          styleTextUnderTheLoader: const TextStyle(),
          loaderColor: cPrimaryLight),
      routes: <String, WidgetBuilder>{
        "/": (BuildContext context) =>
            const WelcomeScreen(title: "Welcome to Readington.X"),
        "/login": (BuildContext context) => const LoginScreen(title: "Login"),
        "/fpword": (BuildContext context) => const ForgotPasswordScreen(),
        "/fpwordverify": (BuildContext context) =>
            const ForgotPasswordVerifyScreen(),
        "/signup": (BuildContext context) =>
            const SignUpScreen(title: "Sign Up"),
        "/profile": (BuildContext context) =>
            const ProfileScreen(title: 'Profile'),
        "/homework": (BuildContext context) =>
            const HomeWorkScreen(title: 'Homework'),
        "/quizzes": (BuildContext context) =>
            const QuizScreen(title: 'Quizzes'),
        "/gradebook": (BuildContext context) =>
            const GradebookScreen(title: 'Gradebook'),
        "/rank": (BuildContext context) => const RankScreen(title: 'Ranking'),
        "/meetings": (BuildContext context) =>
            const MeetingsScreen(title: 'Meetings'),
        "/forum": (BuildContext context) => const ForumsScreen(title: 'Forum'),
      },
    );
  }
}

dynamic SplashScreen({
  required int seconds,
  required WelcomeScreen navigateAfterSeconds,
  required Text title,
  required MaterialColor backgroundColor,
  required TextStyle styleTextUnderTheLoader,
  required Color loaderColor,
}) {}

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  set height(double height) {}

  @override
  Widget build(BuildContext context) {
    BoxHeightStyle.includeLineSpacingTop;
    return (Expanded(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      const Text(
        'Welcome to $appTitle',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: cPrimary,
        ),
        textAlign: TextAlign.center,
      ),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        AppButton(
            text: "LOGIN",
            press: () {
              Navigator.pushNamed(context, "/login");
            }),
        AppButton(
            text: "SIGN UP",
            press: () {
              Navigator.pushNamed(context, "/signup");
            }),
      ]),
    ])));
  }
}

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key, required Comparable<String> title});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          leading: BackButton(onPressed: () {
            Navigator.pop(context);
          }),
          backgroundColor: cPrimary,
          centerTitle: true,
          title: const Text(appTitle),
        ),
        body: const LoginForm(),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({super.key});

  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  String email = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          appLogo(),
          InputField(
            hintText: 'Email Address',
            onChanged: (value) {
              setState(() {
                email = value;
              });
            },
          ),
          PasswordField(
            onChanged: (value) {
              setState(() {
                password = value;
              });
            },
            hintText: '',
          ),
          AppButton(
              press: () async {
                if (_formKey.currentState!.validate()) {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return const HomeScreen(
                      title: 'Dashboard',
                    );
                  }));
                }
                print(email);
                print(password);
              },
              text: 'LOGIN'),
          TextButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const SignUpScreen(
                  title: 'Sign Up',
                );
              }));
            },
            child: const Text(
                "Don't have an account? Create an account, click here."),
          ),
        ],
      ),
    );
  }
}

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({super.key, required Comparable<String> title});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          leading: BackButton(onPressed: () {
            Navigator.pop(context);
          }),
          backgroundColor: cPrimary,
          centerTitle: true,
          title: const Text(appTitle),
        ),
        body: const SignUpForm(),
      ),
    );
  }
}


class ForgotPasswordScreen extends StatelessWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          leading: BackButton(onPressed: () {
            Navigator.pop(context);
          }),
          backgroundColor: cPrimary,
          centerTitle: true,
          title: const Text("$appTitle Forgot Password"),
        ),
        body: const ForgotPasswordForm(),
      ),
    );
  }
}

class ForgotPasswordForm extends StatefulWidget {
  const ForgotPasswordForm({super.key});

  @override
  ForgotPasswordFormState createState() {
    return ForgotPasswordFormState();
  }
}

class ForgotPasswordFormState extends State<ForgotPasswordForm> {
  final _formKey = GlobalKey<FormState>();

  String email = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          appLogo(),
          InputField(
            hintText: 'Email Address',
            onChanged: (value) {
              setState(() {
                email = value;
              });
            },
          ),
          AppButton(
              press: () {
                if (_formKey.currentState!.validate()) {
                  Navigator.pushNamed(context, "/fpwordverify");
                }
                print(email);
              },
              text: 'RESET PASSWORD'),
          TextButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const SignUpScreen(
                  title: 'Sign Up',
                );
              }));
            },
            child: const Text(
                "Don't have an account? Create an account, click here."),
          ),
        ],
      ),
    );
  }
}

class ForgotPasswordVerifyScreen extends StatelessWidget {
  const ForgotPasswordVerifyScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: appTitle,
        home: Scaffold(
          appBar: AppBar(
            leading: BackButton(onPressed: () {
              Navigator.pop(context);
            }),
            backgroundColor: cPrimary,
            centerTitle: true,
            title: const Text("$appTitle Forgot Password"),
          ),
          body: const Center(
            child: Text(
              "If your details match our records, an email with a link will be sent to you.",
              style: TextStyle(fontSize: 24.0, color: cPrimary),
            ),
          ),
        ));
  }
}

class SignUpForm extends StatefulWidget {
  const SignUpForm({super.key});

  @override
  SignUpFormState createState() {
    return SignUpFormState();
  }
}

class SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  String first_name = "";
  String last_name = "";
  String email = "";
  String date_of_birth = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          appLogo(),
          InputField(
            hintText: "First Name",
            onChanged: (value) {
              setState(() {
                first_name = value;
              });
            },
          ),
          InputField(
            hintText: "Last Name",
            onChanged: (value) {
              setState(() {
                last_name = value;
              });
            },
          ),
          InputField(
            hintText: "Date of Birth",
            onChanged: (value) {
              setState(() {
                date_of_birth = value;
              });
            },
          ),
          InputField(
            hintText: "Your Email",
            onChanged: (value) {
              setState(() {
                email = value;
              });
            },
          ),
          PasswordField(
            onChanged: (value) {
              setState(() {
                password = value;
              });
            },
            hintText: "Password",
          ),
          AppButton(
              text: "CREATE AN ACCOUNT",
              press: () async {
                if (_formKey.currentState!.validate()) {
                  FirebaseFirestore.instance
                      .collection("users")
                      .add({"First Name": first_name, "Last Name": last_name, "Birth date": date_of_birth, "Email Address": email, "Password": password}).then((value) => 
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return const LoginScreen(
                      title: "Login",
                    );
                  })));
                }
                print(first_name);
                print(last_name);
                print(date_of_birth);
                print(email);
                print(password);
              }),
          TextButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const LoginScreen(
                  title: 'Login',
                );
              }));
            },
            child: const Text("Already have an account? Sign in here."),
          ),
        ]));
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(appTitle),
        backgroundColor: cPrimary,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'Welcome to your $title!',
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Log Out',
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return const LoginScreen(
              title: 'Login',
            );
          }));
        },
        backgroundColor: cPrimary,
        child: const Icon(Icons.logout),
      ),
      drawer: const AppDrawer(),
    );
  }
}

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key, required this.title});
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
        backgroundColor: cPrimary,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const UserAvatar(),
          InputField(
            hintText: "First Name",
            onChanged: (value) {},
          ),
          InputField(
            hintText: "Last Name",
            onChanged: (value) {},
          ),
          InputField(
            hintText: "Date of Birth",
            onChanged: (value) {},
          ),
          InputField(
            hintText: "Your Email",
            onChanged: (value) {},
          ),
          PasswordField(
            onChanged: (value) {},
            hintText: "Password",
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Back',
          onPressed: () {
            Navigator.pop(context);
          },
          backgroundColor: cPrimary,
          child: const Icon(Icons.arrow_back_rounded)),
      drawer: const AppDrawer(),
    );
  }
}

class HomeWorkScreen extends StatelessWidget {
  const HomeWorkScreen({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
        backgroundColor: cPrimary,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          appLogo(),
       Center(
          child: 
         Text(
            'Here lies where your $title will be',
            style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
          color: cPrimary)
          ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Back',
          onPressed: () {
            Navigator.pop(context);
          },
          backgroundColor: cPrimary,
          child: const Icon(Icons.arrow_back_rounded)),
      drawer: const AppDrawer(),
    );
  }
}

class QuizScreen extends StatelessWidget {
  const QuizScreen({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
        backgroundColor: cPrimary,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          appLogo(),
         Center(
          child: 
         Text(
            'Here lies where your $title will be',
            style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
          color: cPrimary)
          ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Back',
          onPressed: () {
            Navigator.pop(context);
          },
          backgroundColor: cPrimary,
          child: const Icon(Icons.arrow_back_rounded)),
      drawer: const AppDrawer(),
    );
  }
}

class GradebookScreen extends StatelessWidget {
  const GradebookScreen({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
        backgroundColor: cPrimary,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          appLogo(),
           Center(
          child: 
         Text(
            'Here lies where your $title will be',
            style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
          color: cPrimary)
          ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Back',
          onPressed: () {
            Navigator.pop(context);
          },
          backgroundColor: cPrimary,
          child: const Icon(Icons.arrow_back_rounded)),
      drawer: const AppDrawer(),
    );
  }
}

class RankScreen extends StatelessWidget {
  const RankScreen({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
        backgroundColor: cPrimary,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          appLogo(),
            Center(
          child: 
         Text(
            'Here lies where your $title will be',
            style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
          color: cPrimary)
          ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Back',
          onPressed: () {
            Navigator.pop(context);
          },
          backgroundColor: cPrimary,
          child: const Icon(Icons.arrow_back_rounded)),
      drawer: const AppDrawer(),
    );
  }
}

class MeetingsScreen extends StatelessWidget {
  const MeetingsScreen({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
        backgroundColor: cPrimary,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          appLogo(),
           Center(
          child: 
         Text(
            'Here lies where your $title will be',
            style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
          color: cPrimary)
          ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Back',
          onPressed: () {
            Navigator.pop(context);
          },
          backgroundColor: cPrimary,
          child: const Icon(Icons.arrow_back_rounded)),
      drawer: const AppDrawer(),
    );
  }
}

class ForumsScreen extends StatelessWidget {
  const ForumsScreen({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
        backgroundColor: cPrimary,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          appLogo(),
            Center(
          child: 
         Text(
            'Here lies where your $title will be',
            style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
          color: cPrimary)
          ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Back',
          onPressed: () {
            Navigator.pop(context);
          },
          backgroundColor: cPrimary,
          child: const Icon(Icons.arrow_back_rounded)),
      drawer: const AppDrawer(),
    );
  }
}

//AppButton //StringInputField //PasswordPasField //AppDrawer
class AppButton extends StatelessWidget {
  final String text;
  final Function() press;
  final Color color, textColor;
  final double? height;
  const AppButton({
    Key? key,
    required this.text,
    required this.press,
    this.color = cPrimary,
    this.textColor = cPrimaryLight,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      height: height ?? size.height * 0.055,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(29),
        child: newElevatedButton(),
      ),
    );
  }

  Widget newElevatedButton() {
    return ElevatedButton(
      onPressed: press,
      style: ElevatedButton.styleFrom(
        primary: cPrimary,
        padding: const EdgeInsets.symmetric(
          horizontal: 40,
        ),
        textStyle: const TextStyle(
          color: cPrimaryLight,
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
      ),
      child: Text(
        text,
        style: const TextStyle(
          color: cPrimaryLight,
        ),
      ),
    );
  }
}

class InputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const InputField({
    Key? key,
    required this.hintText,
    this.icon = Icons.person,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        validator: (value) {
          if (value == null || value.isNotEmpty) {
            return 'Please enter valid details';
          }
          return null;
        },
        onChanged: onChanged,
        cursorColor: cPrimary,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: cPrimary,
          ),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 150, vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: cPrimaryLight,
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}

class PasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const PasswordField({
    Key? key,
    required this.onChanged,
    required Comparable<String> hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        obscureText: true,
        onChanged: onChanged,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter a valid password';
          }
          return null;
        },
        cursorColor: cPrimaryLight,
        decoration: const InputDecoration(
          hintText: "Password",
          icon: Icon(
            Icons.lock,
            color: cPrimary,
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: cPrimary,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class UserAvatar extends StatelessWidget {
  const UserAvatar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CircleAvatar(
      backgroundImage: AssetImage('images/RM.png'),
      radius: 75,
      backgroundColor: cPrimaryLight,
    );
  }
}

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // ignore: sort_child_properties_last
      child: ListView(
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: cPrimary,
            ),
            child: UserAvatar(),
          ),
          ListTile(
            leading: const Icon(
              Icons.house,
              color: cPrimary,
            ),
            title: const Text('Home',
                style: TextStyle(fontSize: 24.0, color: cPrimary)),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const HomeScreen(
                  title: "Dashboard",
                );
              }));
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.person,
              color: cPrimary,
            ),
            title: const Text(
              'Profile',
              style: TextStyle(fontSize: 24.0, color: cPrimary),
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const ProfileScreen(
                  title: "Profile",
                );
              }));
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.menu_book,
              color: cPrimary,
            ),
            title: const Text('Homework',
                style: TextStyle(fontSize: 24.0, color: cPrimary)),
            onTap: () {
              Navigator.pushNamed(context, "/homework");
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.question_mark_rounded,
              color: cPrimary,
            ),
            title: const Text('Quizzes',
                style: TextStyle(fontSize: 24.0, color: cPrimary)),
            onTap: () {
              Navigator.pushNamed(context, "/quizzes");
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.my_library_books,
              color: cPrimary,
            ),
            title: const Text('Gradebook',
                style: TextStyle(fontSize: 24.0, color: cPrimary)),
            onTap: () {
              Navigator.pushNamed(context, "/gradebook");
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.scoreboard_sharp,
              color: cPrimary,
            ),
            title: const Text('Ranking',
                style: TextStyle(fontSize: 24.0, color: cPrimary)),
            onTap: () {
              Navigator.pushNamed(context, "/rank");
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.calendar_month_outlined,
              color: cPrimary,
            ),
            title: const Text('Meetings',
                style: TextStyle(fontSize: 24.0, color: cPrimary)),
            onTap: () {
              Navigator.pushNamed(context, "/meetings");
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.forum_outlined,
              color: cPrimary,
            ),
            title: const Text('Forums',
                style: TextStyle(fontSize: 24.0, color: cPrimary)),
            onTap: () {
              Navigator.pushNamed(context, "/forums");
            },
          ),
        ],
      ),
      backgroundColor: cPrimaryLight,
    );
  }
}

///NEW
CircleAvatar appLogo() {
  return const CircleAvatar(
    backgroundImage: AssetImage('images/favicon.png'),
    radius: 100,
    backgroundColor: cPrimaryLight,
  );
}